# Description:

Weather app developed in React Native with Expo

# API used:

OpenWeatherMap - http://openweathermap.org/api

Google Place Autocomplete/Places API - https://developers.google.com/places/web-service/autocomplete

